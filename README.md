# Description
2D Puzzle Platformer with some surprises and a litte touch of metroidvania elements.

Play on Itch.io: https://delphoenyx.itch.io/oworld (no download requiered)

Twitch VODs of this game's development: https://youtube.com/playlist?list=PLismT7k3B0-2-djnV2asJSTK3Ntt-VIo9

Unity Version: 2021.3.4f1

![](media/images/OworldTitleScreen.jpg)

![](media/images/level_01.jpg)

![](media/images/level_07.jpg)

![](media/images/oworldSecretArea1.jpg)

![](media/images/oworldBigGem.jpg)
