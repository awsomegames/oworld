using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinToObject : MonoBehaviour
{
    public Transform objectToPinTo;
    public Vector3 offset;

    private void Update()
    {
        transform.position = objectToPinTo.position + offset;
    }
}
