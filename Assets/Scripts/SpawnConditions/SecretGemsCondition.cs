using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SecretGemsCondition", menuName = "Spawn Conditions/SecretGemsCondition", order = 1)]
public class SecretGemsCondition : SpawnCondition
{
    public override bool Condition()
    {
        return ProgressManager.Instance.hasSecretGemsBeenActivated;
    }
}