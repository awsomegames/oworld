using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ChangeColorIfSpawnConditionAttached))]
public class SpawnOnCondition : MonoBehaviour
{
    public List<SpawnCondition> spawnConditions;
    void Start()
    {
        SpriteRenderer spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        if(AllSpawnConditionsAreSatisfied()) {
            spriteRenderer.color = Color.white;
        }
        else {
            Destroy(gameObject);
        }
    }

    public bool AllSpawnConditionsAreSatisfied() {
        if (spawnConditions == null || spawnConditions.Count == 0) {
            Debug.LogWarning("No conditions defined!");
            return false;
        }
        foreach(SpawnCondition spawnCondition in spawnConditions) {
            if(!spawnCondition.Condition()) {
                return false;
            }
        }
        return true;
    }
}
