using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class SpawnCondition : ScriptableObject {
    public abstract bool Condition();
}