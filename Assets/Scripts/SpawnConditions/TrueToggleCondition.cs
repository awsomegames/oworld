using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TrueCondition", menuName = "Spawn Conditions/True Condition", order = 1)]
public class TrueToggleCondition : SpawnCondition
{
    public bool isTrue;
    public override bool Condition()
    {
        return isTrue;
    }
}