using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ChangeColorIfSpawnConditionAttached : MonoBehaviour
{
    public Color color = Color.white - new Color(0, 0, 0, 0.5f);
    void Start()
    {
        if(GetComponent<SpawnOnCondition>() != null) {
            SpriteRenderer spriteRenderer = GetComponentInChildren<SpriteRenderer>();
            spriteRenderer.color = color;
        }
        else {
            Debug.LogWarning("No SpawnOnCondition script added to this object!");
        }
    }
}
