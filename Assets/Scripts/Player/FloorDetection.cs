using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDetection : MonoBehaviour
{
    [Header("Booleans")]
    public bool isOnFloor = false;

    public event Action<ObjectProperties> OnLand;
    public event Action<ObjectProperties> WhileOnFloor;

    #region Collisions Callbacks
    private void OnTriggerEnter2D(Collider2D collider)
    {
        ObjectProperties objProperties = collider.GetComponent<ObjectProperties>();
        if(objProperties != null) {
            if(objProperties.canBeJumpedOn) {
                    if(!isOnFloor) {
                    OnLand?.Invoke(objProperties);
                    isOnFloor = true;
                }
                else {
                    WhileOnFloor?.Invoke(objProperties);
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        ObjectProperties objProperties = collider.GetComponent<ObjectProperties>();
        if(objProperties != null) {
            if(objProperties.canBeJumpedOn) {
                    if(!isOnFloor) {
                    OnLand?.Invoke(objProperties);
                    isOnFloor = true;
                }
                else {
                    WhileOnFloor?.Invoke(objProperties);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        ObjectProperties floor = other.GetComponent<ObjectProperties>();
        if(floor != null) {
            if(floor.canBeJumpedOn) {
                isOnFloor = false;
            }
        }
    }
    #endregion
}
