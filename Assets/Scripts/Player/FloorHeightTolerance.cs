using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorHeightTolerance : MonoBehaviour
{
    public Transform origin;
    public float distance;
    private PlayerMovement playerMovement;

    private void Awake()
    {
        playerMovement = GetComponentInParent<PlayerMovement>();
    }

    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(origin.position, Vector2.down, distance);
        Debug.DrawRay(origin.position, Vector3.down * distance, Color.yellow, Time.deltaTime * 1.1f);
        if(hit.collider !=  null) {
            Debug.Log("Collision!");
            ObjectProperties objectProperties = hit.collider.GetComponent<ObjectProperties>();
            if(objectProperties != null) {
                if(objectProperties.canBeJumpedOn) {
                    // TODO: Move player a little up
                    playerMovement.transform.position = new Vector3(
                        playerMovement.transform.position.x,
                        playerMovement.transform.position.y + (distance - hit.distance),
                        playerMovement.transform.position.z
                    );
                }
            }
        }
    }
}
