using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    #region References
    public Rigidbody2D rbody;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public FloorDetection floorDetection;
    public GameObject spriteTrailPrefab;
    #endregion

    #region  Movement Variables
    public float jumpPower;
    public float walkSpeed;
    public float dashSpeed;
    [Tooltip("Dash speed duration in seconds.")]
    public float dashDuration;
    [Tooltip("Interval in seconds at wich sprite trail will appear when player is dashing.")]
    public float dashTrailInterval;
    public float canDashEffectSpeedMultiplier;
    public float canDashEffectInterval;
    public Color dashTrailColor;
    public float gravitySwitchTrailInterval;
    public Color gravitySwitchTrailColor;
    #endregion

    #region  Private Variables
    private float inputMoveValue;
    private bool hasReachedGoal = false;
    private bool isDead = false;
    private bool isDashing = false;
    public bool hasDash { get; private set; } = false;
    private bool isSwitchingGravity = false;
    private SoundManager soundManager;
    #endregion

    #region Coroutine Variables
    private Coroutine canDashEffectCoroutine = null;
    private Coroutine gravitySwitchTrailCoroutine = null;
    #endregion

    #region Context Menu
    [ContextMenu("Recommended Values/All")]
    public void _RecommendedValues_All(){
        _RecommendedValues_Jump();
        _RecommendedValues_Walk();
        _RecommendedValues_Dash();
        _RecommendedValues_DashVisualEffects();
        _RecommendedValues_GravitySwitchVisualEffects();
    }

    [ContextMenu("Recommended Values/Jump")]
    public void _RecommendedValues_Jump(){
        jumpPower = 12.3f;
    }

    [ContextMenu("Recommended Values/Walk")]
    public void _RecommendedValues_Walk(){
        walkSpeed = 6f;
    }

    [ContextMenu("Recommended Values/Dash")]
    public void _RecommendedValues_Dash(){
        dashSpeed = 20f;
        dashDuration = 0.2f;
    }

    [ContextMenu("Recommended Values/Dash visual effects")]
    public void _RecommendedValues_DashVisualEffects(){
        dashTrailInterval = 0.03f;
        canDashEffectSpeedMultiplier = 3f;
        canDashEffectInterval = 0.02f;
    }

    [ContextMenu("Recommended Values/Gravity switch visual effects")]
    public void _RecommendedValues_GravitySwitchVisualEffects(){
        gravitySwitchTrailInterval = 0.06f;
    }
    #endregion

    #region Unity Life Cycle
    private void Awake() {
        soundManager = GetComponent<SoundManager>();
    }

    private void Start() {
        floorDetection.OnLand += OnLand;
        floorDetection.WhileOnFloor += WhileOnFloor;
    }

    private void Update() {
        if (GameManager.Instance.isGamePaused) { return; }
        CheckVerticalSpeed();
        CheckHorizontalSpeed();
        if(CanMove()) {
            if(!isDashing) {
                Walk(inputMoveValue);
                FlipOrientation();
            }
        }
    }
    #endregion

    #region Input Listeners
    private void OnJump() {
        if(CanJump()) {
            Jump();
        }
    }

    private void OnMove(InputValue inputValue) {
        inputMoveValue = inputValue.Get<float>();
    }

    private void OnDash() {
        if (isDashing) { return; }
        if(CanDash()) {
            Dash();
        }
    }

    private bool CanDash() {
        return !isDead && hasDash && !hasReachedGoal;
    }

    private void OnRestartLevel() {
        if(!hasReachedGoal){
            SceneManagerScript.Instance.ReloadLevel();
        }
    }
    #endregion

    #region Jump Methods
    private bool CanJump() {
        return CanMove() && floorDetection.isOnFloor;
    }

    private void Jump() {
        rbody.velocity = new Vector2(rbody.velocity.x, jumpPower * -Mathf.Sign(Physics2D.gravity.y));
        soundManager.PlayClipByNameOnWolrd("Jump");
    }
    #endregion

    #region Movement Methods
    private void Walk(float direction) {
        rbody.velocity = new Vector2(walkSpeed * direction, rbody.velocity.y);
    }

    public void StopHorizontalMovement() {
        Walk(0);
    }

    public void ReachedGoal() {
        hasReachedGoal = true;
    }

    public bool CanMove() {
        return
        !isDead &&
        !hasReachedGoal &&
        !isSwitchingGravity;
    }

    private void FlipOrientation() {
        if(inputMoveValue != 0) {
            FlipOrientationTo(-inputMoveValue);
        }
    }

    public void FlipOrientationTo(float direction) {
        transform.localScale = new Vector3(
            direction,
            transform.localScale.y,
            transform.localScale.z
        );
    } 
    #endregion

    #region Dash Methods
    public event System.Action OnDashPerformed;
    private void Dash() {
        StartCoroutine(DashCoroutine(-transform.localScale.x, dashDuration));
        StartCoroutine(DashTrailCoroutine(dashDuration, dashTrailInterval));
        DeactivateDash();
        OnDashPerformed?.Invoke();
        soundManager.PlayClipByNameOnWolrd("Dash");
    }

    private IEnumerator DashCoroutine(float direction, float duration) {
        isDashing = true;
        rbody.velocity = new Vector2(dashSpeed * direction, 0);
        float originalGravityScale = rbody.gravityScale;
        rbody.gravityScale = 0;
        yield return new WaitForSeconds(duration);
        isDashing = false;
        rbody.gravityScale = originalGravityScale;
    }

    private IEnumerator DashTrailCoroutine(float duration, float interval) {
        for (float time = 0; time <= duration; time += interval)
        {
            // TODO: Spawn trail
            GameObject obj = Instantiate(spriteTrailPrefab, transform.position, Quaternion.identity);
            SpriteRenderer objSpriteRenderer = obj.GetComponent<SpriteRenderer>();
            objSpriteRenderer.sprite = spriteRenderer.sprite;
            objSpriteRenderer.color = dashTrailColor;
            // TODO: Wait interval
            yield return new WaitForSeconds(interval);
        }
        yield return null;
    }

    public void ActivateDash() {
        hasDash = true;
        canDashEffectCoroutine = StartCoroutine(CanDashEffectCoroutine(canDashEffectInterval));
    }

    private void DeactivateDash() {
        hasDash = false;
        if(canDashEffectCoroutine != null) {
            StopCoroutine(canDashEffectCoroutine);
        }
    }

    private IEnumerator CanDashEffectCoroutine(float interval)
    {
        while(true) {
            Vector3 offset = new Vector3(
                Random.Range(-0.1f, 0.1f),
                Random.Range(-0.1f, 0.1f),
                0
            );
            GameObject obj;
            if(Mathf.Abs(rbody.velocity.x) < 0.1f && Mathf.Abs(rbody.velocity.y) < 0.1f) {
                obj = Instantiate(spriteTrailPrefab, transform.position + offset, Quaternion.identity);
            }
            else {
                obj = Instantiate(spriteTrailPrefab, transform.position, Quaternion.identity);
            }
            SpriteRenderer objSpriteRenderer = obj.GetComponent<SpriteRenderer>();
            objSpriteRenderer.sprite = spriteRenderer.sprite;
            objSpriteRenderer.color = dashTrailColor;
            Animator objAnimator = obj.GetComponent<Animator>();
            objAnimator.speed = canDashEffectSpeedMultiplier;
            yield return new WaitForSeconds(interval);
        }
    }
    #endregion

    #region Check animation speed variables
    private void CheckHorizontalSpeed() {
        if(Mathf.Abs(rbody.velocity.x) <= 0.1) {
            animator.SetBool("isHorizontallyMoving", false);
        }
        else {
            animator.SetBool("isHorizontallyMoving", true);
        }
    }

    private void CheckVerticalSpeed() {
        if(Mathf.Abs(rbody.velocity.y) <= 0.1) {
            animator.SetBool("isVerticallyMoving", false);
        }
        else {
            animator.SetBool("isVerticallyMoving", true);
        }
    }
    #endregion

    #region Gravity Switching
    private IEnumerator GravitySwitchTrailCoroutine(float interval) {
        while(true)
        {
            // TODO: Spawn trail
            GameObject obj = Instantiate(spriteTrailPrefab, transform.position, Quaternion.identity);
            SpriteRenderer objSpriteRenderer = obj.GetComponent<SpriteRenderer>();
            objSpriteRenderer.sprite = spriteRenderer.sprite;
            objSpriteRenderer.color = gravitySwitchTrailColor;
            // TODO: Wait interval
            yield return new WaitForSeconds(interval);
        }
    }

    public void SwitchedGravity() {
        isSwitchingGravity = true;
        floorDetection.isOnFloor = false;
        StopHorizontalMovement();
        if(gravitySwitchTrailCoroutine != null) {
            StopCoroutine(gravitySwitchTrailCoroutine);
        }
        gravitySwitchTrailCoroutine = StartCoroutine(GravitySwitchTrailCoroutine(gravitySwitchTrailInterval));
    }
    #endregion

    #region Floor Methods
    private void OnLand(ObjectProperties objectProperties) {
        isSwitchingGravity = false;
        if(gravitySwitchTrailCoroutine != null) {
            StopCoroutine(gravitySwitchTrailCoroutine);
        }
    }

    private void WhileOnFloor(ObjectProperties objectProperties) {
        // TODO: ???
    }
    #endregion

    public void Die(float forceMultiplier = 1f) {
        soundManager.PlayClipByNameOnWolrd("Death");
        isDead = true;
        rbody.velocity = Vector2.zero;
        Collider2D [] playerColliders = GetComponentsInChildren<Collider2D>();
        foreach(Collider2D collider in playerColliders) {
            collider.enabled = false;
        }
        rbody.constraints = RigidbodyConstraints2D.None;
        int randomSign = (int)Mathf.Sign(Random.Range(-10, 10));
        Vector2 randomForce = new Vector2(Random.Range(1, 4) * randomSign, Random.Range(5, 10) * -Mathf.Sign(Physics2D.gravity.y));
        randomForce *= forceMultiplier;
        rbody.AddForceAtPosition(
            randomForce,
            transform.position - Vector3.down,
            ForceMode2D.Impulse
        );
    }
}