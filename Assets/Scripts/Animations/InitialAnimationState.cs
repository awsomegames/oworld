using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialAnimationState : MonoBehaviour
{
    private Animator animator;
    public bool initialAnimation = false;
    public List<string> initialAnimationsNames;
    public bool initialParameters = false;
    public List<BooleanStateByName> initialParametersState;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        if(initialAnimation) {
            foreach(string animName in initialAnimationsNames) {
                animator.Play(animName);
            }
        }
        if(initialParameters) {
            foreach(BooleanStateByName stateByName in initialParametersState) {
                animator.SetBool(stateByName.name, stateByName.value);
            }
        }
    }
}
