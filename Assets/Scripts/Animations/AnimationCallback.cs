using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum AnimationEventKeyboardKey {
    Finished,
    EndPress,
    FadedOut,
    FadedIn,
    NONE
}

[RequireComponent(typeof(Animator))]
public class AnimationCallback : MonoBehaviour
{
    [HideInInspector] public Animator animator;
    public event Action<string> OnAnimationMessage;
    public event Action<AnimationEventKeyboardKey> OnAnimationEventKeyboardKey;
    [HideInInspector] public AnimationEventKeyboardKey lastEventReached = AnimationEventKeyboardKey.NONE;
    [HideInInspector] public string lastMessageSent = "";

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void AnimationMessage(string message) {
        OnAnimationMessage?.Invoke(message);
        lastMessageSent = message;
    }

    public void AnimationEventType(AnimationEventKeyboardKey message) {
        OnAnimationEventKeyboardKey?.Invoke(message);
        lastEventReached = message;
    }

    public bool HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey eventType) {
        if(lastEventReached == eventType) {
            lastEventReached = AnimationEventKeyboardKey.NONE;
            return true;
        }
        return false;
    }
}