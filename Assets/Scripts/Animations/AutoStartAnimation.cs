using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum DelayType {
        Random,
        Set
}
[System.Serializable]
public struct MinMax {
    public float min;
    public float max;
}
public class AutoStartAnimation : MonoBehaviour
{
    public Animator animator;
    public string animationName;

    public DelayType delayType;
    public float setDelay;

    public MinMax minMaxDelay;

    private void Start() {
        float delay = 0;
        switch(delayType){
            case DelayType.Random:
                delay = Random.Range(minMaxDelay.min, minMaxDelay.max);
                break;
            case DelayType.Set:
                delay = setDelay;
                break;
        }
        StartCoroutine(StartAnimationCoroutine(animationName, delay));
    }

    private IEnumerator StartAnimationCoroutine(string animationName, float delay) {
        yield return new WaitForSeconds(delay);
        animator.Play(animationName);
        yield return null;
    }

}
