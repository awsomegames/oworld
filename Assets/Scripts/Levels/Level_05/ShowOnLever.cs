using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnLever : MonoBehaviour
{
    public Interactable lever;
    public Animator animator;
    public bool initialAnimation = false;
    public List<string> initialAnimationsNames;
    public bool initialParameters = false;
    public List<BooleanStateByName> initialParametersState;


    private void Start()
    {
        lever.PlayerGetsInRange += Show;
        lever.PlayerGetsOutRange += Hide;
        if(initialAnimation) {
            foreach(string animName in initialAnimationsNames) {
                animator.Play(animName);
            }
        }
        if(initialParameters) {
            foreach(BooleanStateByName stateByName in initialParametersState) {
                animator.SetBool(stateByName.name, stateByName.value);
            }
        }
    }

    private void Show(){
        animator.SetBool("PlayerInRange", true);
    }

    private void Hide(){
        animator.SetBool("PlayerInRange", false);
    }
}
