using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardAnimationController : MonoBehaviour
{
    [Header("WASD")]
    [SerializeField] private AnimationCallback letterW;
    [SerializeField] private AnimationCallback letterA;
    [SerializeField] private AnimationCallback letterS;
    [SerializeField] private AnimationCallback letterD;
    [SerializeField] private AnimationCallback spacebar;
    private List<AnimationCallback> wasdKeys;
    [Header("Arrows")]
    [SerializeField] private AnimationCallback arrowUp;
    [SerializeField] private AnimationCallback arrowLeft;
    [SerializeField] private AnimationCallback arrowDown;
    [SerializeField] private AnimationCallback arrowRight;
    [SerializeField] private AnimationCallback letterC;
    private List<AnimationCallback> arrowKeys;

    private void Awake()
    {
        wasdKeys = new List<AnimationCallback>();
        wasdKeys.Add(letterW);
        wasdKeys.Add(letterA);
        wasdKeys.Add(letterS);
        wasdKeys.Add(letterD);
        wasdKeys.Add(spacebar);

        arrowKeys = new List<AnimationCallback>();
        arrowKeys.Add(arrowUp);
        arrowKeys.Add(arrowLeft);
        arrowKeys.Add(arrowDown);
        arrowKeys.Add(arrowRight);
        arrowKeys.Add(letterC);
    }

    private void Start()
    {
        StartCoroutine(KeyboardAnimationCoroutine());
    }

    private void FadeInKeys(List<AnimationCallback> keys, bool instant = false) {
        float startTime = 0;
        if(instant) {
            startTime = 1;
        }
        foreach(AnimationCallback ac in keys){
            ac.animator.Play("FadeIn", -1, startTime);
        }
    }

    private void FadeOutKeys(List<AnimationCallback> keys, bool instant = false) {
        float startTime = 0;
        if(instant) {
            startTime = 1;
        }
        foreach(AnimationCallback ac in keys){
            ac.animator.Play("FadeOut", -1, startTime);
        }
    }

    private IEnumerator KeyboardAnimationCoroutine() {
        FadeOutKeys(wasdKeys, instant: true);
        FadeInKeys(arrowKeys);
        while (true)
        {    
            yield return new WaitUntil(() => arrowDown.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.FadedIn));
            yield return StartCoroutine(ArrowsAnimationCoroutine());
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(ArrowsAnimationCoroutine());
            FadeOutKeys(arrowKeys);
            yield return new WaitUntil(() => arrowDown.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.FadedOut));
            FadeInKeys(wasdKeys);
            yield return new WaitUntil(() => spacebar.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.FadedIn));
            yield return StartCoroutine(WASDAnimationCoroutine());
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(WASDAnimationCoroutine());
            FadeOutKeys(wasdKeys);
            yield return new WaitUntil(() => spacebar.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.FadedOut));
            FadeInKeys(arrowKeys);
        }
    }

    private IEnumerator WASDAnimationCoroutine()
    {
        letterA.animator.Play("Press");
        yield return new WaitUntil(() => letterA.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.EndPress));
        letterA.animator.Play("Still");
        letterD.animator.Play("Press"); 
        yield return new WaitUntil(() => letterD.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.EndPress));
        letterD.animator.Play("Still");
        letterW.animator.Play("Press");
        spacebar.animator.Play("Press");
        yield return new WaitUntil(() => spacebar.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.EndPress));
        letterW.animator.Play("Still");
        spacebar.animator.Play("Still");
        yield return null;
    }

    private IEnumerator ArrowsAnimationCoroutine()
    {
        arrowLeft.animator.Play("Press");
        yield return new WaitUntil(() => arrowLeft.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.EndPress));
        arrowLeft.animator.Play("Still");
        arrowRight.animator.Play("Press");
        yield return new WaitUntil(() => arrowRight.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.EndPress));
        arrowRight.animator.Play("Still");
        arrowUp.animator.Play("Press");
        letterC.animator.Play("Press");
        yield return new WaitUntil(() => letterC.HasTriggeredAnimationEventKeyboardKey(AnimationEventKeyboardKey.EndPress));
        arrowUp.animator.Play("Still");
        letterC.animator.Play("Still");
        yield return null;
    }
}
