using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreenScript : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public Button playButton;
    public SoundManager soundManager;
    [Header("OWORLD Letters")]
    public List<Animator> letterAnimators;
    public float delayToDance;
    public float lettersDanceInterval;

    private void Start()
    {
        StartCoroutine(DanceLettersOrderCoroutine(delayToDance, lettersDanceInterval));
    }

    public void ClickedOnPlay(){
        MovePlayerToRight();
        playButton.interactable = false;
        soundManager.PlayCurrentClip();
    }
    public void MovePlayerToRight() {
        playerMovement.ReachedGoal();
        playerMovement.rbody.velocity = new Vector2(
            playerMovement.walkSpeed,
            playerMovement.rbody.velocity.y
        );
        playerMovement.FlipOrientationTo(-1);
    }

    public IEnumerator DanceLettersOrderCoroutine(float startDelay, float interval) {
        yield return new WaitForSeconds(startDelay);
        foreach(Animator animator in letterAnimators) {
            animator.Play("LetterDanceStart");
            yield return new WaitForSeconds(interval);
        }
        yield return null;
    }
}
