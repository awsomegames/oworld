using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LevelLoadType {
    LoadNextLevel,
    ReloadLevel,
    TitleScreen,
    CustomLevel
}

[RequireComponent(typeof(Collider2D))]
public class SendToLevelCollider : MonoBehaviour
{
    [Tooltip("Delay in seconds before reloading scene.")]
    [SerializeField] private protected float delay;
    [Tooltip("If false, it will load the next level according to the SceneManagerScript.")]
    [SerializeField] protected LevelLoadType levelLoadType;
    [SerializeField] protected string sceneToLoad;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player")) {
            PlayerMovement player = other.GetComponent<PlayerMovement>();
            PlayerEntered(player);
        }
    }

    protected virtual void PlayerEntered(PlayerMovement player) {
        player.StopHorizontalMovement();
        switch(levelLoadType) {
            case LevelLoadType.LoadNextLevel:
                SceneManagerScript.Instance.LoadNextLevel(delay);
                break;
            case LevelLoadType.ReloadLevel:
                SceneManagerScript.Instance.ReloadLevel(delay);
                break;
            case LevelLoadType.TitleScreen:
                SceneManagerScript.Instance.LoadTitleScreen(delay);
                break;
            case LevelLoadType.CustomLevel:
                SceneManagerScript.Instance.LoadLevelBySceneName(sceneToLoad, delay);
                break;
        }
    }
}
