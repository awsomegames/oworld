using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class BigGemEvent : MonoBehaviour
{
    [SerializeField] private DashGem bigGem;
    [SerializeField] private Animator globalLightAnimator;
    [SerializeField] private ParticleSystem gemParticles;
    [SerializeField] private float delay;
    [SerializeField] private SoundManager soundManager;

    private void Start()
    {
        bigGem.OnCollect += GemEventCinematic;
    }

    private void GemEventCinematic()
    {
        StartCoroutine(GemEventCinematicCoroutine());
    }

    private IEnumerator GemEventCinematicCoroutine() {
        ProgressManager.Instance.hasSecretGemsBeenActivated = true;
        soundManager.PlayClipByName("Spark");
        globalLightAnimator.Play("Shine");
        Instantiate(gemParticles, bigGem.transform.position, Quaternion.identity);
        SceneManagerScript.Instance.LoadTitleScreen(delay);
        yield return new WaitForSeconds(delay - 1.5f);
        soundManager.PlayClipByName("Win");
    }
}
