using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigGemScript : MonoBehaviour
{
    [SerializeField] private float animatorSpeedMultiplier = 0.5f;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        animator.speed = animatorSpeedMultiplier;
    }
}
