using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BooleanStateByName {
    public bool value;
    public string name;
}

public class ShowOnPlayer : MonoBehaviour
{
    public Collectable gem;
    public PlayerMovement player;
    public Animator animator;
    public DestroyObject destroyObject;
    public bool initialAnimation = false;
    public List<string> initialAnimationsNames;
    public bool initialParameters = false;
    public List<BooleanStateByName> initialParametersState;

    private void Start()
    {
        gem.OnCollect += Show;
        player.OnDashPerformed += Hide;
        if(initialAnimation) {
            foreach(string animName in initialAnimationsNames) {
                animator.Play(animName);
            }
        }
        if(initialParameters) {
            foreach(BooleanStateByName stateByName in initialParametersState) {
                animator.SetBool(stateByName.name, stateByName.value);
            }
        }
    }

    private void Show(){
        animator.SetBool("PlayerInRange", true);
    }

    private void Hide(){
        if(animator.GetBool("PlayerInRange")) {
            animator.SetBool("PlayerInRange", false);
            destroyObject.DestroyThisObject(1f);
        }
    }

    private void OnDestroy()
    {
        gem.OnCollect -= Show;
        player.OnDashPerformed -= Hide;
    }

}
