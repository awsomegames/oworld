using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraFocus : MonoBehaviour
{
    [SerializeField] private Cinemachine.CinemachineVirtualCamera cinemachine;
    [SerializeField] private Transform focusTarget;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player")) {
            cinemachine.Follow = focusTarget;
        }
    }
}
