using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AutoStartAnimation))]
public class AutoStartAnimationEditor : Editor
{
    #region Serialized Properties
    SerializedProperty space;
    SerializedProperty animator;
    SerializedProperty animationName;
    SerializedProperty delayType;
    SerializedProperty setDelay;
    SerializedProperty minMaxDelay;
    #endregion

    #region Foldout Bools
    bool foldoutDelay;
    #endregion

    #region Constants
    private const float spaceBeforeHeader = 5;
    private const float spaceAfterHeader = 0;
    #endregion


    public void OnEnable()
    {
        space = serializedObject.FindProperty("space");
        animator = serializedObject.FindProperty("animator");
        animationName = serializedObject.FindProperty("animationName");
        delayType = serializedObject.FindProperty("delayType");
        setDelay = serializedObject.FindProperty("setDelay");
        minMaxDelay = serializedObject.FindProperty("minMaxDelay");

        foldoutDelay = true;
    }

    public override void OnInspectorGUI()
    {
        DelayType delayTypeValue = (DelayType)delayType.enumValueIndex;

        MyHeader("References", spaceBeforeHeader: 0);
        EditorGUILayout.PropertyField(animator);

        MyHeader("Animation");
        EditorGUILayout.PropertyField(animationName);

        foldoutDelay = EditorGUILayout.BeginFoldoutHeaderGroup(foldoutDelay, "Delay");
        if(foldoutDelay) {
            EditorGUILayout.PropertyField(delayType);
            if(delayTypeValue == DelayType.Set) {
                EditorGUILayout.PropertyField(setDelay);
            }
            if(delayTypeValue == DelayType.Random) {
                EditorGUILayout.PropertyField(minMaxDelay);
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void MyHeader(string text ,float spaceBeforeHeader = spaceBeforeHeader, float spaceAfterHeader = spaceAfterHeader) {
        EditorGUILayout.Space(spaceBeforeHeader);
        EditorGUILayout.LabelField(text, EditorStyles.boldLabel);
        EditorGUILayout.Space(spaceAfterHeader);
    }
    
}
