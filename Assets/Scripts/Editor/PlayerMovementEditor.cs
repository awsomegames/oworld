
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using EditorUtils;

[CustomEditor(typeof(PlayerMovement))]
public class PlayerMovementEditor : Editor
{
    MyFields utils;

    #region Serialized Properties
    SerializedProperty rbody;
	SerializedProperty animator;
	SerializedProperty spriteRenderer;
	SerializedProperty floorDetection;
	SerializedProperty spriteTrailPrefab;
	SerializedProperty jumpPower;
	SerializedProperty walkSpeed;
	SerializedProperty dashSpeed;
	SerializedProperty dashDuration;
	SerializedProperty dashTrailInterval;
	SerializedProperty canDashEffectSpeedMultiplier;
	SerializedProperty canDashEffectInterval;
	SerializedProperty dashTrailColor;
	SerializedProperty gravitySwitchTrailInterval;
	SerializedProperty gravitySwitchTrailColor;
	SerializedProperty inputMoveValue;
	SerializedProperty hasReachedGoal;
	SerializedProperty isDead;
	SerializedProperty isDashing;
	SerializedProperty canDash;
	SerializedProperty isSwitchingGravity;
	SerializedProperty soundManager;
    #endregion

    #region Foldout Bools
    private bool foldReferences;
    private bool foldMovement;
    #endregion

    bool advancedSettings = false;


    public void OnEnable()
    {
        utils = new MyFields();

        rbody = serializedObject.FindProperty("rbody");
		animator = serializedObject.FindProperty("animator");
		spriteRenderer = serializedObject.FindProperty("spriteRenderer");
		floorDetection = serializedObject.FindProperty("floorDetection");
		spriteTrailPrefab = serializedObject.FindProperty("spriteTrailPrefab");
		jumpPower = serializedObject.FindProperty("jumpPower");
		walkSpeed = serializedObject.FindProperty("walkSpeed");
		dashSpeed = serializedObject.FindProperty("dashSpeed");
        dashDuration = serializedObject.FindProperty("dashDuration");
        dashTrailInterval = serializedObject.FindProperty("dashTrailInterval");
		canDashEffectSpeedMultiplier = serializedObject.FindProperty("canDashEffectSpeedMultiplier");
		canDashEffectInterval = serializedObject.FindProperty("canDashEffectInterval");
		dashTrailColor = serializedObject.FindProperty("dashTrailColor");
		gravitySwitchTrailInterval = serializedObject.FindProperty("gravitySwitchTrailInterval");
		gravitySwitchTrailColor = serializedObject.FindProperty("gravitySwitchTrailColor");
		inputMoveValue = serializedObject.FindProperty("inputMoveValue");
		hasReachedGoal = serializedObject.FindProperty("hasReachedGoal");
		isDead = serializedObject.FindProperty("isDead");
		isDashing = serializedObject.FindProperty("isDashing");
		canDash = serializedObject.FindProperty("canDash");
		isSwitchingGravity = serializedObject.FindProperty("isSwitchingGravity");
		soundManager = serializedObject.FindProperty("soundManager");

        foldReferences = true;
        foldMovement = true;
    }

    public override void OnInspectorGUI()
    {
        GUI.enabled = false;
        EditorGUILayout.ObjectField(
            "Script",
            MonoScript.FromMonoBehaviour((PlayerMovement)target),
            typeof(PlayerMovement),
            false
        );
        GUI.enabled = true;

        foldReferences = BeginFold(foldReferences, "References");
        if(foldReferences) {
            EditorGUILayout.PropertyField(rbody);
            EditorGUILayout.PropertyField(animator);
            EditorGUILayout.PropertyField(spriteRenderer);
            EditorGUILayout.PropertyField(floorDetection);
            EditorGUILayout.PropertyField(spriteTrailPrefab);
            utils.MyHorizontalLine();
            //HorizontalLine(Color.gray);
        }
        EndFold();

        advancedSettings = utils.MyToggle(advancedSettings, "Show Advanced settings");

        foldMovement = BeginFold(foldMovement, "Movement");
        if(foldMovement) {
            EditorGUILayout.PropertyField(jumpPower);
            EditorGUILayout.PropertyField(walkSpeed);
            EditorGUILayout.PropertyField(dashSpeed);
            EditorGUILayout.PropertyField(dashDuration);
            utils.MyHeader("Dash visual effects");
            EditorGUILayout.PropertyField(dashTrailColor);
            if (advancedSettings) {
                EditorGUILayout.PropertyField(dashTrailInterval);
                EditorGUILayout.PropertyField(canDashEffectSpeedMultiplier);
                EditorGUILayout.PropertyField(canDashEffectInterval);
            }
            utils.MyHorizontalLine();
            //HorizontalLine(Color.gray);
        }
        EndFold();

        utils.MyHeader("Gravity Switching");
        if (advancedSettings) {
            EditorGUILayout.PropertyField(gravitySwitchTrailInterval);
        }
        EditorGUILayout.PropertyField(gravitySwitchTrailColor);

        serializedObject.ApplyModifiedProperties();
    }

    private bool BeginFold(bool b, string text) => EditorGUILayout.BeginFoldoutHeaderGroup(b, text);
    private void EndFold() => EditorGUILayout.EndFoldoutHeaderGroup();

}
