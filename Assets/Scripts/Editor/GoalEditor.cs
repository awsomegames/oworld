using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using EditorUtils;

[CustomEditor(typeof(Goal))]
public class GoalEditor : Editor
{
    MyFields utils;

    #region Serialized Properties
	SerializedProperty delay;
	SerializedProperty changesGravityOnTouch;
	SerializedProperty changesGravityTo;
    SerializedProperty levelLoadType;
    SerializedProperty sceneToLoad;

    #endregion

    #region Foldout Bools
    #endregion

    public void OnEnable()
    {
        utils = new MyFields();

		delay = serializedObject.FindProperty("delay");
		changesGravityOnTouch = serializedObject.FindProperty("changesGravityOnTouch");
		changesGravityTo = serializedObject.FindProperty("changesGravityTo");
        levelLoadType = serializedObject.FindProperty("levelLoadType");
        sceneToLoad = serializedObject.FindProperty("sceneToLoad");
    }

    public override void OnInspectorGUI()
    {
        GUI.enabled = false;
        EditorGUILayout.ObjectField(
            "Script",
            MonoScript.FromMonoBehaviour((Goal)target),
            typeof(Goal),
            false
        );
        GUI.enabled = true;

		EditorGUILayout.PropertyField(delay);
		EditorGUILayout.PropertyField(changesGravityOnTouch);
        if(changesGravityOnTouch.boolValue) {
            EditorGUILayout.PropertyField(changesGravityTo);
        }
		EditorGUILayout.PropertyField(levelLoadType);
        if(levelLoadType.enumValueIndex == (int)LevelLoadType.CustomLevel) {
            EditorGUILayout.PropertyField(sceneToLoad);
        }

        serializedObject.ApplyModifiedProperties();
    }

    private bool BeginFold(bool b, string text) => EditorGUILayout.BeginFoldoutHeaderGroup(b, text);
    private void EndFold() => EditorGUILayout.EndFoldoutHeaderGroup();
    
}