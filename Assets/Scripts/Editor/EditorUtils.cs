using UnityEditor;
using UnityEngine;

namespace EditorUtils {
    public class MyFields {
        public float spaceBeforeHeader = 5;
        public float spaceAfterHeader = 0;
        public void MyHeader(string text) {
            EditorGUILayout.Space(spaceBeforeHeader);
            EditorGUILayout.LabelField(text, EditorStyles.boldLabel);
            EditorGUILayout.Space(spaceAfterHeader);
        }

        public void MyHeader(string text, float spaceBefore, float spaceAfter) {
            EditorGUILayout.Space(spaceBefore);
            EditorGUILayout.LabelField(text, EditorStyles.boldLabel);
            EditorGUILayout.Space(spaceAfter);
        }

        public void MyHorizontalLine() {
            MyHeader("_____________________________");
        }

        public bool MyToggle(bool b, string text) {
            EditorGUILayout.BeginHorizontal();
            GUIStyle italicLabelStyle = new GUIStyle(GUI.skin.label);
            italicLabelStyle.fontStyle = FontStyle.Italic;

            Color originalGUIColor = GUI.color;
            GUI.color = Color.cyan;
            EditorGUILayout.LabelField(text, italicLabelStyle);
            GUI.color = originalGUIColor;

            b = EditorGUILayout.Toggle(b);
            EditorGUILayout.EndHorizontal();
            return b;
        }
    }
}