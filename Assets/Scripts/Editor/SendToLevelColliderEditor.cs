using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using EditorUtils;

[CustomEditor(typeof(SendToLevelCollider), true)]
public class SendToLevelColliderEditor : Editor
{
    MyFields utils;

    #region Serialized Properties
	SerializedProperty delay;
	SerializedProperty levelLoadType;
	SerializedProperty sceneToLoad;

    #endregion

    #region Foldout Bools
    #endregion

    public void OnEnable()
    {
        utils = new MyFields();

		delay = serializedObject.FindProperty("delay");
		levelLoadType = serializedObject.FindProperty("levelLoadType");
		sceneToLoad = serializedObject.FindProperty("sceneToLoad");

    }

    public override void OnInspectorGUI()
    {
        GUI.enabled = false;
        EditorGUILayout.ObjectField(
            "Script",
            MonoScript.FromMonoBehaviour((SendToLevelCollider)target),
            typeof(SendToLevelCollider),
            false
        );
        GUI.enabled = true;

		EditorGUILayout.PropertyField(delay);
		EditorGUILayout.PropertyField(levelLoadType);
        if(levelLoadType.enumValueIndex == (int)LevelLoadType.CustomLevel) {
            EditorGUILayout.PropertyField(sceneToLoad);
        }
		
        serializedObject.ApplyModifiedProperties();
    }

    private bool BeginFold(bool b, string text) => EditorGUILayout.BeginFoldoutHeaderGroup(b, text);
    private void EndFold() => EditorGUILayout.EndFoldoutHeaderGroup();
    
}