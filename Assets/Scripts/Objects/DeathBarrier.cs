using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathBarrier : MonoBehaviour
{
    [Tooltip("Delay in seconds before reloading scene.")]
    public float delay;
    
    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        if(collider2D.CompareTag("Player")){
            collider2D.GetComponent<PlayerMovement>().Die(forceMultiplier: 0f);
            SceneManagerScript.Instance.ReloadLevel(delay);
        }
        else {
            Destroy(collider2D.gameObject);
        }
    }

}
