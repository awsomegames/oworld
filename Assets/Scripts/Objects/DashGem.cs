using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashGem : Collectable
{
    protected override void OnPlayerCollision(PlayerMovement player)
    {
        if(!player.hasDash) {
            Collect(player);
        }
    }

    protected override void Collected(PlayerMovement player)
    {
        player.ActivateDash();
    }
}
