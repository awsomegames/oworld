using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public void DestroyThisObject() {
        Destroy(gameObject);
    }

    public void DestroyThisObject(float delay) {
        StartCoroutine(DestroyThisObjectCoroutine(delay));
    }

    private IEnumerator DestroyThisObjectCoroutine(float delay) {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}
