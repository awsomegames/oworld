using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityLever : Interactable
{
    [Header("References")]
    public Animator animator;

    private GravityLever[] levers;
    private SoundManager soundManager;

    private void Awake()
    {
        soundManager = GetComponent<SoundManager>();
    }

    private void Start()
    {
        levers = GameObject.FindObjectsOfType<GravityLever>();
    }

    public override void OnInteraction()
    {
        LevelManager.Instance.SwitchGravity();
        PlayLeversAnimations();
        PlaySound();
    }

    public override void PlayerGotInRange()
    {
        return;
    }

    public override void PlayerGotOutRange()
    {
        return;
    }

    private void PlaySound() {
        if(Mathf.Sign(Physics2D.gravity.y) < 0) {
            soundManager.SetPitch(0.8f);
        }
        else {
            soundManager.SetPitch(1f);
        }
        soundManager.PlayCurrentClip();
    }

    private void PlayLeversAnimations() {
        if(Mathf.Sign(Physics2D.gravity.y) < 0) {
            foreach(GravityLever lever in levers) {
                lever.animator.Play("Base.LeverDeactivated");
            }
        }
        else {
            foreach(GravityLever lever in levers) {
                lever.animator.Play("Base.LeverActivated");
            }
        }
    }
}
