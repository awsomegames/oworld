using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spikes : MonoBehaviour
{
    [Tooltip("Delay in seconds before reloading scene.")]
    public float delay;

    private void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        if(collisionInfo.collider.CompareTag("Player")) {
            PlayerMovement playerMovement = collisionInfo.collider.GetComponent<PlayerMovement>();
            KillPlayer(playerMovement);
        }
    }

    private void KillPlayer(PlayerMovement playerMovement) {
        playerMovement.Die();
        SceneManagerScript.Instance.ReloadLevel(delay);
    }
}
