using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectProperties : MonoBehaviour
{
    [Header("Floor")]
    public bool canBeJumpedOn = true;
    [Header("Gravity")]
    public bool flipsOnGravitySwitch = false;
}   
