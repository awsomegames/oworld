using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    [Header("Booleans")]
    public bool isPlayerOnRange = false;
    public event Action PlayerGetsInRange;
    public event Action PlayerGetsOutRange;
    public event Action OnPlayerInteraction;

    private void OnInteract() {
        if(isPlayerOnRange) {
            OnPlayerInteraction?.Invoke();
            OnInteraction();
        }
    }

    public abstract void OnInteraction();
    public abstract void PlayerGotInRange();
    public abstract void PlayerGotOutRange();

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        if(collider2D.CompareTag("Player")) {
            PlayerGetsInRange?.Invoke();
            isPlayerOnRange = true;
            PlayerGotInRange();
        }
    }

    private void OnTriggerExit2D(Collider2D collider2D)
    {
        if(collider2D.CompareTag("Player")) {
            PlayerGetsOutRange?.Invoke();
            isPlayerOnRange = false;
            PlayerGotOutRange();
        }
    }
}
