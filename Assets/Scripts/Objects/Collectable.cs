using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Collectable : MonoBehaviour
{
    public ParticleSystem particles;
    public SpriteRenderer spriteRenderer;
    public Collider2D objectCollider;
    public float destroyingDelay;

    public event Action OnCollect;

    protected SoundManager soundManager;
    
    private void Awake() {
        soundManager = GetComponent<SoundManager>();
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        if(collider2D.CompareTag("Player")) {
            PlayerMovement player = collider2D.GetComponent<PlayerMovement>();
            OnPlayerCollision(player);
        }
    }

    protected virtual void OnPlayerCollision(PlayerMovement player) {
        Collect(player);
    }

    protected void CollectEffect() {
        StartCoroutine(CollectEffectCoroutine());
    }

    protected virtual IEnumerator CollectEffectCoroutine() {
        spriteRenderer.enabled = false;
        objectCollider.enabled = false;
        particles.Play();
        yield return new WaitForSeconds(destroyingDelay);
        Destroy(gameObject);
        yield return null;
    }

    protected virtual void Collect(PlayerMovement player) {
        CollectEffect();
        soundManager.PlayCurrentClip();
        Collected(player);
        OnCollect?.Invoke();
    }

    protected abstract void Collected(PlayerMovement player);
}
