using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : SendToLevelCollider
{
    public bool changesGravityOnTouch;
    [Range(-1, 1)]
    public int changesGravityTo;

    private SoundManager soundManager;

    private void Awake()
    {
        soundManager = GetComponent<SoundManager>();
    }

    protected override void PlayerEntered(PlayerMovement player)
    {
        LevelManager.Instance.goalReached = true;
        if(changesGravityOnTouch) {
            LevelManager.Instance.SetGravity(changesGravityTo);
        }
        player.ReachedGoal();
        soundManager.PlayCurrentClip();
        base.PlayerEntered(player);
        StartCoroutine(MovePlayerToCenterCoroutine(player, 1f));
        SceneManagerScript.Instance.CompleteLevel();
    }

    private IEnumerator MovePlayerToCenterCoroutine(PlayerMovement player, float moveTime) {
        Vector3 distance = transform.position - player.transform.position;
        player.rbody.velocity = new Vector2(distance.x, 0);
        player.transform.localScale = new Vector3(
            Mathf.Abs(player.transform.localScale.x) * -Mathf.Sign(distance.x),
            player.transform.localScale.y,
            player.transform.localScale.z
        );
        yield return new WaitForSeconds(moveTime);
        player.StopHorizontalMovement();
        yield return null;
    }

}
