using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public bool isGamePaused { get; private set; } = false;

    private void Awake()
    {
        if(Instance == null) {
            Instance = this;
            //DontDestroyOnLoad(this);
        }
        else {
            Destroy(gameObject);
        }
    }

    public void OnPause() {
        if (SceneManagerScript.Instance.isPlayerOnTitleScreen) { return; }
        TogglePause();
    }

    public void PauseGame() {
        isGamePaused = true;
        Time.timeScale = 0;
        UIManager.LocalInstance.ShowUpPauseMenu();
    }

    public void UnpauseGame() {
        isGamePaused = false;
        Time.timeScale = 1;
        UIManager.LocalInstance.HidePauseMenu();
    }

    public void TogglePause() {
        if(isGamePaused) {
            UnpauseGame();
        }
        else {
            PauseGame();
        }
    }
}
