using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManager : MonoBehaviour
{
    public bool hasPlatformEffector = true;
    public List<PlatformEffector2D> platformEffectors2D;

    private List<Transform> gravityFlippableObjects;

    private void Start()
    {
        ObtainAllFlippableObjects();
        SetGravity(-1, deactivatePlayersMovement: false);
    }

    private void ObtainAllFlippableObjects() {
        gravityFlippableObjects = new List<Transform>();
        ObjectProperties [] allObjects = GameObject.FindObjectsOfType<ObjectProperties>();
        foreach(ObjectProperties obj in allObjects) {
            if(obj.flipsOnGravitySwitch) {
                gravityFlippableObjects.Add(obj.transform);
            }
        }
    }

    public void SwitchGravity() {
        SetGravity((int)Mathf.Sign(Physics2D.gravity.y * -1));
    }

    private void DeactivatePlayersMovement() {
        PlayerMovement[] players = GameObject.FindObjectsOfType<PlayerMovement>();
        foreach(PlayerMovement player in players) {
            player.SwitchedGravity();
        }
    }

    private void FlipAllObjects(int gravityDirection) {
        foreach(Transform t in gravityFlippableObjects){
            FlipVertical(t, gravityDirection);
        }
    }

    private void FlipVertical(Transform t, int gravityDirection) {
        t.localScale = new Vector3(t.localScale.x,
                                   Mathf.Abs(t.localScale.y) * -gravityDirection,
                                   t.localScale.z);
    }

    public void SetGravity(int gravityValue, bool deactivatePlayersMovement = true) {
        int gravityDirection = (int)Mathf.Sign(gravityValue);
        if(deactivatePlayersMovement) {
            DeactivatePlayersMovement();
        }
        FlipAllObjects(gravityDirection);
        Physics2D.gravity = new Vector2(0, Mathf.Abs(Physics2D.gravity.y) * gravityDirection);
        if(hasPlatformEffector) {
            if(gravityValue < 0) {
                foreach(PlatformEffector2D effector in platformEffectors2D){
                    effector.rotationalOffset = 0;
                }
            }
            else if(gravityValue > 0) {
                foreach(PlatformEffector2D effector in platformEffectors2D){
                    effector.rotationalOffset = 180;
                }
            }
        }
    }
}
