using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelInfo {
    public string name;
    public string sceneName;
    //public SceneAsset scene;
    public bool seen = false;
    public bool completed = false;
}

public class SceneManagerScript : MonoBehaviour
{
    public static SceneManagerScript Instance;
    [SerializeField] private string defaultScene = "";
    public string titleScreenName = "TitleScreen";
    [Tooltip("Levels in their proper order.")]
    public LevelInfo currentLevel = null;
    public List<LevelInfo> levels;
    private int currentLevelIndex = 0;
    public bool isPlayerOnTitleScreen => SceneManager.GetActiveScene().name == titleScreenName;

    private void Awake()
    {
        if(Instance == null) {
            Instance = this;
            DontDestroyOnLoad(Instance.transform.parent);
            SceneManager.sceneLoaded += OnSceneLoad;
            levels.RemoveAt(0);
        }
        else {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        currentLevel = GetLevelBySceneName(SceneManager.GetActiveScene().name);
        currentLevelIndex = IndexOflLevelBySceneName(SceneManager.GetActiveScene().name);
    }

    private void OnSceneLoad(Scene scene, LoadSceneMode loadSceneMode) {
        currentLevel = GetLevelBySceneName(scene.name);
        GameManager.Instance.UnpauseGame();
        currentLevelIndex = IndexOflLevelBySceneName(scene.name);
        MarkLevelAsSeen(currentLevel.sceneName);
        UIManager.LocalInstance.UpdateLevelName(currentLevel.name);
    }

    private int IndexOflLevelBySceneName(string sceneName) {
        int i = 0;
        foreach(LevelInfo levelInfo in levels) {
            if (levelInfo.sceneName == sceneName) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private LevelInfo GetLevelByLevelName(string levelName) {
        foreach(LevelInfo levelInfo in levels) {
            if (levelInfo.name == levelName) {
                return levelInfo;
            }
        }
        return null;
    }

    private LevelInfo GetLevelBySceneName(string sceneName) {
        foreach(LevelInfo levelInfo in levels) {
            if (levelInfo.sceneName == sceneName) {
                return levelInfo;
            }
        }
        return null;
    }

    private void ReloadScene() {
        string sceneToLoad = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneToLoad);
    }

    private IEnumerator ReloadSceneCoroutine(float delay = 0) {
        yield return new WaitForSeconds(delay);
        ReloadScene();
        yield return null;
    }

    public void ReloadLevel(float delay = 0) {
        StartCoroutine(LoadSceneByNameCoroutine("", delay));
    }

    public void LoadNextLevel(float delay = 0)
    {
        int nextLevelIndex = IndexOflLevelBySceneName(SceneManager.GetActiveScene().name) + 1;
        string nextLevelName = "";
        try {
            nextLevelName = levels[nextLevelIndex].sceneName;
        }
        catch(System.ArgumentOutOfRangeException ex) {
            Debug.LogWarning("No more levels in list!\n" + ex.Message);
            StartCoroutine(LoadSceneByNameCoroutine(defaultScene, delay));
            return;
        }

        StartCoroutine(LoadSceneByNameCoroutine(nextLevelName, delay));
        currentLevelIndex++;
    }

    public void LoadLevelBySceneName(string sceneName, float delay = 0)
    {
        StartCoroutine(LoadSceneByNameCoroutine(sceneName, delay));
    }

    public void LoadLevelByLevelName(string levelName, float delay = 0)
    {
        string sceneName = GetLevelByLevelName(levelName).sceneName;
        StartCoroutine(LoadSceneByNameCoroutine(sceneName, delay));
    }

    private IEnumerator LoadSceneByNameCoroutine(string nextLevelName, float delay) {
        yield return new WaitForSeconds(delay);
        LoadSceneByName(nextLevelName);
        yield return null;
    }

    private void LoadSceneByName(string nextLevelName) {
        string sceneToLoad = nextLevelName;
        if(sceneToLoad == "") {
            sceneToLoad = SceneManager.GetActiveScene().name;
        }
        SceneManager.LoadScene(sceneToLoad);
    }

    public void LoadTitleScreen(float delay = 0) {
        StartCoroutine(LoadSceneByNameCoroutine(titleScreenName, delay));
    }

    public bool HasLevelBeenCompleted(string levelName){
        LevelInfo levelInfo = GetLevelByLevelName(levelName);
        return levelInfo.completed;
    }

    public bool HasLevelBeenSeen(string levelName){
        LevelInfo levelInfo = GetLevelByLevelName(levelName);
        return levelInfo.seen;
    }

    public void CompleteLevel(string sceneName){
        LevelInfo levelInfo = GetLevelBySceneName(sceneName);
        levelInfo.completed = true;
    }

    public void CompleteLevel(){
        CompleteLevel(SceneManager.GetActiveScene().name);
    }

    public void MarkLevelAsSeen(string sceneName) {
        LevelInfo levelInfo = GetLevelBySceneName(sceneName);
        levelInfo.seen = true;
    }

}
