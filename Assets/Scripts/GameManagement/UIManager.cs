using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager LocalInstance;
    [SerializeField] private PauseMenu pauseMenu;
    [SerializeField] private TMP_Text levelName;

    private void Awake()
    {
        LocalInstance = this;
    }

    public void ShowUpPauseMenu() {
        pauseMenu.ShowUp();
    }

    public void HidePauseMenu() {
        pauseMenu.Hide();
    }

    public void UpdateLevelName(string newLevelName) {
        levelName.text = newLevelName;
    }
}
