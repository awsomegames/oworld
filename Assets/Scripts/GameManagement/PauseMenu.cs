using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    private SoundManager soundManager;
    [SerializeField] private TMP_Dropdown dropdownLevelSelection;
    [SerializeField] private TMP_Text infoLabel;
    private Animator infoLabelAnimator;

    private void Awake()
    {
        soundManager = GetComponent<SoundManager>();
        infoLabelAnimator = infoLabel.GetComponent<Animator>();
        infoLabelAnimator.updateMode = AnimatorUpdateMode.UnscaledTime;
    }

    private void Start()
    {
        InitLevelOptions();
    }

    public void ShowUp() {
        gameObject.SetActive(true);
        soundManager.PlayCurrentClip();
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void GoToTitleScreen() {
        SceneManagerScript.Instance.LoadTitleScreen();
    }

    public void UnpauseGame() {
        GameManager.Instance.UnpauseGame();
    }

    public void GoToSelectedLevel() {
        int value = dropdownLevelSelection.value;
        string selectedOption = dropdownLevelSelection.options[value].text;
        if(SceneManagerScript.Instance.HasLevelBeenSeen(selectedOption)) {
            SceneManagerScript.Instance.LoadLevelByLevelName(selectedOption);
        }
        else {
            infoLabel.text = "You haven't seen that level!";
            infoLabelAnimator.Play("SideShake", -1, 0);
        }
    }

    private void InitLevelOptions() {
        dropdownLevelSelection.ClearOptions();
        TMP_Dropdown.OptionData currentOption = null;
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
        foreach(LevelInfo levelInfo in SceneManagerScript.Instance.levels) {
            TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData(levelInfo.name);
            options.Add(option);
            if(levelInfo == SceneManagerScript.Instance.currentLevel) {
                currentOption = option;
            }
        }
        options.RemoveAt(options.Count - 1);
        dropdownLevelSelection.AddOptions(options);

        dropdownLevelSelection.value = dropdownLevelSelection.options.IndexOf(currentOption);
    }
}
