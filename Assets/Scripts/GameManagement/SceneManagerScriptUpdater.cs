
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(SceneManagerScript))]
public class SceneManagerScriptUpdater : MonoBehaviour {
    [SerializeField] private SceneManagerScript sceneManagerScript;
    public void Update() {
        #if UNITY_EDITOR
        // Get scene name and set it
        /*foreach(LevelInfo levelInfo in sceneManagerScript.levels){
            if (levelInfo == null | levelInfo.scene == null || levelInfo.name == "") { continue; }
            SceneAsset sceneAsset = levelInfo.scene;
            string sceneName = sceneAsset.name;
            levelInfo.sceneName = sceneName;
        }*/
        #endif
    }
}