using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public bool goalReached = false;

    private GravityManager gravityManager;

    private void Awake()
    {
        Instance = null;
        Instance = this;

        gravityManager = GetComponent<GravityManager>();
    }

    public void SwitchGravity() {
        gravityManager.SwitchGravity();
    }

    public void SetGravity(int gravityValue) {
        gravityManager.SetGravity(gravityValue);
    }
}
